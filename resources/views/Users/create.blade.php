@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Add New User</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('users.index') }}" title="Go back"> <i class="fas fa-backward "></i> </a>
            </div>
        </div>
    </div>
    @if (Session::get('success'))
        <div class="alert alert-success">
            <p>{{ Session::get('success') }}</p>
        </div>
    @endif

    <form action="{{ route('users.store') }}" method="POST" >
        @csrf

        <div class="row col-lg-6">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <input type="text" name="firstname"
                           class="form-control @error('firstname') is-invalid @enderror"
                           placeholder="First Name"
                    value="{{ old('firstname') }}">
                    @error('firstname')
                    <em class="text-danger">{{ $message }}</em>
                    @enderror
                </div>
                <div class="form-group">
                    <input type="text" name="lastname"
                               class="form-control @error('lastname') is-invalid @enderror"
                           placeholder="Last Name"
                           value="{{ old('lastname') }}">
                    @error('lastname')
                    <em class="text-danger">{{ $message }}</em>
                    @enderror
                </div>
                <div class="form-group">
                    <input type="text" name="email"
                           class="form-control @error('email') is-invalid @enderror"
                           placeholder="Email"
                           value="{{ old('email') }}">
                    @error('email')
                    <em class="text-danger">{{ $message }}</em>
                    @enderror
                </div>
                <div class="form-group">
                    <input type="text" name="phone"
                           class="form-control @error('phone') is-invalid @enderror"
                           placeholder="Phone"
                           value="{{ old('phone') }}">
                    @error('phone')
                    <em class="text-danger">{{ $message }}</em>
                    @enderror
                </div>
                <div class="form-group">
                    <select type="text" name="role"
                            class="form-control @error('role') is-invalid @enderror">
                        <option value="">Role</option>
                        @if ($roles)
                            @foreach($roles as $role)
                                    <option value="{{ $role['name'] }}"
                                            @if(old('role') == $role['name'])selected @endif>
                                        {{ $role['name'] }}
                                    </option>
                            @endforeach
                        @endif
                    </select>
                    @error('role')
                    <em class="text-danger">{{ $message }}</em>
                    @enderror
                </div>
                <div class="form-group">
                    <input type="password" name="password"
                           class="form-control @error('password') is-invalid @enderror"
                           placeholder="Password">
                    @error('password')
                    <em class="text-danger">{{ $message }}</em>
                    @enderror
                </div>
                <div class="form-group">
                    <input type="checkbox" name="active"
                           class="form-check-input" value="0"
                            @if(old('active')) checked @endif>
                    <label class="form-check-label">Active</label>
                </div>

            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>

    </form>
@endsection
