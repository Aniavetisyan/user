@extends('Layouts.App')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit User</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('users.index') }}" title="Go back"> <i class="fas fa-backward "></i> </a>
            </div>
        </div>
    </div>

    <form action="{{ route('users.update',$user->id) }}" method="POST">
        @csrf
        @method('PUT')

        <div class="row  col-lg-6">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <input type="text" name="firstname"
                           value="{{ old('firstname',$user->firstname) }}"
                           class="form-control  @error('firstname') is-invalid @enderror"
                           placeholder="Firstname">
                    @error('firstname')
                    <em class="text-danger">{{ $message }}</em>
                    @enderror
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <input type="text" name="lastname"
                           value="{{ old('lastname',$user->lastname) }}"
                           class="form-control  @error('lastname') is-invalid @enderror"
                           placeholder="Lastname">
                    @error('lastname')
                    <em class="text-danger">{{ $message }}</em>
                    @enderror
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <input type="text" name="email"
                           value="{{ old('email',$user->email) }}"
                           class="form-control  @error('email') is-invalid @enderror"
                           placeholder="Email">
                    @error('email')
                    <em class="text-danger">{{ $message }}</em>
                    @enderror
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <input type="text" name="phone"
                           value="{{ old('phone',$user->phone) }}"
                           class="form-control  @error('phone') is-invalid @enderror"
                           placeholder="Phone">
                    @error('phone')
                    <em class="text-danger">{{ $message }}</em>
                    @enderror
                </div>
            </div>
            <div class="form-group">
                <select type="text" name="role"
                        class="form-control @error('role') is-invalid @enderror">
                    <option value="">Role</option>
                    @if ($roles)
                        @foreach($roles as $role)
                            <option value="{{ $role['name'] }}"
                                    @if($user->role == $role['name'])selected @endif>
                                {{ $role['name'] }}
                            </option>
                        @endforeach
                    @endif
                </select>
                @error('role')
                <em class="text-danger">{{ $message }}</em>
                @enderror
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <input type="password" name="password"
                           class="form-control  @error('password') is-invalid @enderror"
                           placeholder="Password">
                    @error('password')
                    <em class="text-danger">{{ $message }}</em>
                    @enderror
                </div>
            </div>
            <div class="form-group">
                <input type="checkbox" name="active"
                       class="form-check-input"
                       @if($user->active || old('active')) checked @endif>
                <label class="form-check-label">Active</label>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>

    </form>
@endsection
