$(document).ready(function() {
    let isChecked = $('input[name="active"]');
    isChecked.is(':checked') ? isChecked.val(1) : isChecked.val(0);
    $('input[name="active"]').on('change',function(){
        if($(this).is(':checked')){
            $(this).val(1);
        }
        else {
            $(this).val(0);
        }
    })
});
