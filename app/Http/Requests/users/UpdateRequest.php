<?php

namespace App\Http\Requests\users;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => ['required','min:3','max:50'],
            'lastname'  => ['required','min:3','max:50'],
            'email'     => ['required','max:50','regex:/(.+)@(.+)\.(.+)/i', Rule::unique('users')->ignore($this->user->id)],
            'phone'     => ['required','regex:/^([0-9\s\-\+\(\)]*)$/','min:8','max:20'],
            'password'  => ['required','min:8','max:64'],
            'role'      => ['required'],
        ];
    }
}
